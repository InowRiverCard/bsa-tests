insert into users values ('d95da7b7-c5c9-479f-a74e-a6523e6cf1b9', 'John'),
('eb52cd63-7e9c-4684-8c20-b3e60504133d', 'Alex'),
('b23abb19-faad-44de-9324-8ec18aa331da', 'Pavel'),
('0a36f5af-c63e-4cfd-930a-6d56754d7b79', 'Anna');


insert into tasks
(id, text, user_id)
values
('110', 'write code', 'd95da7b7-c5c9-479f-a74e-a6523e6cf1b9'),
('111', 'clean room', 'd95da7b7-c5c9-479f-a74e-a6523e6cf1b9'),
('112', 'run cross', 'eb52cd63-7e9c-4684-8c20-b3e60504133d'),
('113', 'go to the gym', 'eb52cd63-7e9c-4684-8c20-b3e60504133d'),
('114', 'write the book', 'eb52cd63-7e9c-4684-8c20-b3e60504133d'),
('115', 'read article', 'b23abb19-faad-44de-9324-8ec18aa331da'),
('116', 'go to the pool', 'b23abb19-faad-44de-9324-8ec18aa331da'),
('117', 'play chess', '0a36f5af-c63e-4cfd-930a-6d56754d7b79');
