package com.example.demo;

import com.example.demo.toDo.ToDoController;

import com.example.demo.user.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationIT {

	@Autowired
	private ToDoController toDoController;

	@Autowired
	private UserController userController;

	@Test
	void contextLoads() throws Exception {
		if (toDoController == null) {
			throw new Exception("ToDoController is null");
		}

		if (userController == null) {
			throw new Exception("UserController is null");
		}
	}

}
