package com.example.demo.toDo;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static org.mockito.Mockito.*;
import com.example.demo.user.UserRepository;
import com.example.demo.user.exeption.UserNotFoundException;
import com.example.demo.user.model.User;
import org.mockito.ArgumentMatchers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.toDo.dto.ToDoSaveRequest;
import com.example.demo.toDo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.toDo.exception.ToDoNotFoundException;
import com.example.demo.toDo.model.ToDoEntity;

class ToDoServiceTest {

	private ToDoRepository toDoRepository;

	private ToDoService toDoService;

	private UserRepository userRepository;


	//executes before each test defined below
	@BeforeEach
	void setUp() {
		this.toDoRepository = mock(ToDoRepository.class);
		this.userRepository = mock(UserRepository.class);
		toDoService = new ToDoService(toDoRepository, userRepository);
	}

	@Test
	void whenGetAll_thenReturnAll() {
		//mock
		var testToDos = new ArrayList<ToDoEntity>();
		testToDos.add(new ToDoEntity(0L, "Test 1"));
		var toDo = new ToDoEntity(1L, "Test 2");
		toDo.completeNow();
		testToDos.add(toDo);
		when(toDoRepository.findAll()).thenReturn(testToDos);

		//call
		var todos = toDoService.getAll();

		//validate
		assertEquals(todos.size(), testToDos.size());
		for (int i = 0; i < todos.size(); i++) {
			assertThat(todos.get(i), samePropertyValuesAs(
				ToDoEntityToResponseMapper.map(testToDos.get(i))
			));
		}
	}

	@Test
	void whenUpsertWithId_thenReturnUpdated() throws ToDoNotFoundException {
		//mock
		var expectedToDo = new ToDoEntity(0L, "New Item");
		when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
			Long id = i.getArgument(0, Long.class);
			if (id.equals(expectedToDo.getId())) {
				return Optional.of(expectedToDo);
			} else {
				return Optional.empty();
			}
		});
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
			Long id = arg.getId();
			if (id != null) {
				if (!id.equals(expectedToDo.getId()))
					return new ToDoEntity(id, arg.getText());
				expectedToDo.setText(arg.getText());
				return expectedToDo; //return valid result only if we get valid id
			} else {
				return new ToDoEntity(40158L, arg.getText());
			}
		});
		
		//call
		var toDoSaveRequest = new ToDoSaveRequest();
		toDoSaveRequest.setId(expectedToDo.getId());
		toDoSaveRequest.setText("Updated Item");
		var todo = toDoService.upsert(toDoSaveRequest);

		//validate
		assertSame(todo.getId(), toDoSaveRequest.getId());
		assertEquals(todo.getText(), toDoSaveRequest.getText());
	}
	
	@Test
	void whenUpsertNoId_thenReturnNew() throws ToDoNotFoundException {
		//mock
		var userId = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
		var user = new User(userId, "user name");
		when(userRepository.findById(ArgumentMatchers.any(UUID.class)))
				.thenReturn(Optional.of(user));

		var newId = 0L;
		when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
			Long id = i.getArgument(0, Long.class);
			if (id == newId) {
				return Optional.empty();
			} else {
				return Optional.of(new ToDoEntity(newId, "Wrong ToDo"));
			}
		});
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
			Long id = arg.getId();
			if (id == null)
				return new ToDoEntity(newId, arg.getText());
			else 
				return new ToDoEntity();
		});

		//call
		var toDoDto = new ToDoSaveRequest();
		toDoDto.setText("Created Item");
		toDoDto.setUserId(userId);
		var result = toDoService.upsert(toDoDto);

		//validate
		assertEquals(result.getId(), newId);
		assertEquals(result.getText(), toDoDto.getText());
	}

	@Test
	void whenUpsertWithWrongUserId_thenThrowUserNotFoundException() {
		//mock
		var userId = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
		when(userRepository.findById(ArgumentMatchers.any(UUID.class)))
				.thenReturn(Optional.empty());

		var toDoDto = new ToDoSaveRequest();
		toDoDto.setText("Created Item");
		toDoDto.setUserId(userId);

		//validate
		assertThrows(UserNotFoundException.class, () ->toDoService.upsert(toDoDto));
	}

	@Test
	void whenComplete_thenReturnWithCompletedAt() throws ToDoNotFoundException {
		var startTime = ZonedDateTime.now(ZoneOffset.UTC);
		//mock
		var todo = new ToDoEntity(0L, "Test 1");
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
			Long id = arg.getId();
			if (id.equals(todo.getId())) {
				return todo;
			} else {
				return new ToDoEntity();
			}
		});

		//call
		var result = toDoService.completeToDo(todo.getId());

		//validate
		assertSame(result.getId(), todo.getId());
		assertEquals(result.getText(), todo.getText());
		assertTrue(result.getCompletedAt().isAfter(startTime));
	}

	@Test
	void whenGetOne_thenReturnCorrectOne() throws ToDoNotFoundException {
		//mock
		var todo = new ToDoEntity(0L, "Test 1");
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));

		//call
		var result = toDoService.getOne(0L);

		//validate
		assertThat(result, samePropertyValuesAs(
			ToDoEntityToResponseMapper.map(todo)
		));
	}

	@Test
	void whenDeleteOne_thenRepositoryDeleteCalled() {
		//call
		var id = 0L;
		toDoService.deleteOne(id);

		//validate
		verify(toDoRepository, times(1)).deleteById(id);
	}

	@Test
	void whenIdNotFound_thenThrowNotFoundException() {
		assertThrows(ToDoNotFoundException.class, () -> toDoService.getOne(1L));
	}

	//my tests
	@Test
	void whenUpsertWithWrongId_thenThrowToDoNotFoundException() {
		//mock
		var wrongToDoSaveRequest = new ToDoSaveRequest();
		wrongToDoSaveRequest.setId(1L);
		wrongToDoSaveRequest.setText("Nonexistent Updated Item");
		wrongToDoSaveRequest.setUserId(UUID.randomUUID());
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());


		//validate
		assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(wrongToDoSaveRequest));
	}

	@Test
	void whenCompleteWithWrongId_thenThrowToDoNotFoundException() {
		//mock
		var wrongId = 1L;
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());

		//validate
		assertThrows(ToDoNotFoundException.class, () -> toDoService.completeToDo(wrongId));
	}

}
