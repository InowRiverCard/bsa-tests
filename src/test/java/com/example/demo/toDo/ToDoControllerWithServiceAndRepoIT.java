package com.example.demo.toDo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 13.07.2020
 */
@AutoConfigureMockMvc
@SpringBootTest
public class ToDoControllerWithServiceAndRepoIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ToDoService toDoService;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "go to the pool";

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(8)))
                .andExpect(jsonPath("$[6].text").value(testText))
                .andExpect(jsonPath("$[6].id").isNumber())
                .andExpect(jsonPath("$[6].completedAt").doesNotExist());
    }

    @Sql(scripts = { "/clean.sql", "/data.sql" }, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Test
    public void whenSaveWithId_thenValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        rootNode.put("id", 110);
        rootNode.put("text", "updated task");
        String queryBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);

        //call
        this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(queryBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value("updated task"))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(110))
                .andExpect(jsonPath("$.completedAt").doesNotExist())
                .andExpect(jsonPath("$.userId").exists());
    }

    @Sql(scripts = { "/clean.sql", "/data.sql" }, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Test
    public void whenSaveWithoutId_thenValidResponse() throws Exception {
        var toDoText = "new task";
        var userId = "b23abb19-faad-44de-9324-8ec18aa331da";
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        rootNode.put("userId", userId);
        rootNode.put("text", toDoText);
        String queryBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);

        //call/valid
        this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(queryBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(toDoText))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.completedAt").doesNotExist())
                .andExpect(jsonPath("$.userId").value(userId));

        assertEquals(3, toDoService.getByUserId(UUID.fromString(userId)).size());
    }


    @Test
    public void whenSaveWithWrongUserIdAndWithoutId_thenReturn404() throws Exception {
        var wrongId = "d95da7b7-c5c9-479f-a74e-a6523e6cf1b0";
        var exceptionMsg = "User not found id=" + wrongId;
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        rootNode.put("userId", wrongId);
        rootNode.put("text", "updated task");
        String queryBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);

        //call
        this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(queryBody))
                .andExpect(status().isNotFound())
                .andExpect(content().string(exceptionMsg));

    }

    @Sql(scripts = { "/clean.sql", "/data.sql" }, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Test
    void whenComplete_thenReturnValidResponse() throws Exception {
        var toDoId = 110L;
        var toDoText = "write code";

        // call/validate
        this.mockMvc
                .perform(put("/todos/" + toDoId +" /complete")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(toDoText))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(110))
                .andExpect(jsonPath("$.completedAt").isNotEmpty());
    }

    @Test
    void whenGetOne_thenReturnCorrectOne() throws Exception {
        var toDoId = 110L;
        var toDoText = "write code";

        // call/validate
        this.mockMvc
                .perform(get("/todos/" + toDoId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(toDoText))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(110))
                .andExpect(jsonPath("$.completedAt").isEmpty());
    }

    @Sql(scripts = { "/clean.sql", "/data.sql" }, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @Test
    void whenDeleteOne_thenRepositoryDeleteCalled() throws Exception {
        var toDoId = 110L;

        // call/validate
        this.mockMvc
                .perform(delete("/todos/" + toDoId))
                .andExpect(status().isOk());

        assertEquals(7, toDoService.getAll().size());
    }

    @Test
    void whenByUserId_thenReturnValidResponse() throws Exception {
        var userId = "eb52cd63-7e9c-4684-8c20-b3e60504133d";

        // call/validate
        this.mockMvc
                .perform(get("/user/todos/" + userId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].text").value("run cross"))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }


}
