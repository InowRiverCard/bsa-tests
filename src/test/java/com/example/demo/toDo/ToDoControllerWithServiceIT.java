package com.example.demo.toDo;

import com.example.demo.user.UserRepository;
import com.example.demo.user.exeption.UserNotFoundException;
import com.example.demo.user.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import com.example.demo.toDo.model.ToDoEntity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;



@ActiveProfiles(profiles = "test")
@WebMvcTest(ToDoController.class)
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoRepository toDoRepository;

	@MockBean
	private UserRepository userRepository;


	@Test
	void whenGetAll_thenReturnValidResponse() throws Exception {
		String testText = "My to do text";
		when(toDoRepository.findAll()).thenReturn(
				Collections.singletonList(
						new ToDoEntity(1L, testText)
				)
		);
		
		this.mockMvc
			.perform(get("/todos"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].text").value(testText))
			.andExpect(jsonPath("$[0].id").isNumber())
			.andExpect(jsonPath("$[0].completedAt").doesNotExist());
	}
	
	@Test
	public void whenSaveWithId_thenValidResponse() throws Exception {

		var userId = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
		var user = new User(userId, "user name");
		var expectedToDo = new ToDoEntity(0L, "New Item");
		expectedToDo.setUser(user);

		when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
			Long id = i.getArgument(0, Long.class);
			if (id.equals(expectedToDo.getId())) {
				return Optional.of(expectedToDo);
			} else {
				return Optional.empty();
			}
		});
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
			Long id = arg.getId();
			if (id != null) {
				if (!id.equals(expectedToDo.getId()))
					return new ToDoEntity(id, arg.getText());
				expectedToDo.setText(arg.getText());
				return expectedToDo; //return valid result only if we get valid id
			} else {
				return new ToDoEntity(40158L, arg.getText());
			}
		});

		var newText = "updated task";
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("id", 0);
		rootNode.put("userId", userId.toString());
		rootNode.put("text", newText);
		String queryBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);

		//call
		this.mockMvc
				.perform(post("/todos")
					.contentType(MediaType.APPLICATION_JSON)
					.content(queryBody))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.text").value(newText))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(0))
				.andExpect(jsonPath("$.completedAt").doesNotExist())
				.andExpect(jsonPath("$.userId").value(userId.toString()));

	}

	@Test
	public void whenSaveWithWrongUserIdAndWithoutId_thenReturn404() throws Exception {
		var exceptionMsg = "wrong id";
		when(userRepository.findById(ArgumentMatchers.any(UUID.class)))
				.thenThrow(new UserNotFoundException(exceptionMsg));
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("userId", "d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
		rootNode.put("text", "updated task");
		String queryBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);

		//call
		this.mockMvc
				.perform(post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(queryBody))
				.andExpect(status().isNotFound())
				.andExpect(content().string(exceptionMsg));

	}

	@Test
	void whenComplete_thenReturnValidResponse() throws Exception {
		//mock
		var toDoId = 0L;
		var toDoText = "Test 1";
		var todo = new ToDoEntity(toDoId, toDoText);
		when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
			Long id = arg.getId();
			if (id.equals(todo.getId())) {
				return todo;
			} else {
				return new ToDoEntity();
			}
		});

		// call/validate
		this.mockMvc
				.perform(put("/todos/" + toDoId +" /complete"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.text").value(toDoText))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(0))
				.andExpect(jsonPath("$.completedAt").isNotEmpty());
	}

}
