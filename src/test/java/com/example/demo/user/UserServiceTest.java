package com.example.demo.user;

import com.example.demo.user.dto.UserCreateDto;
import com.example.demo.user.dto.UserResponseDto;
import com.example.demo.user.dto.UserShortDto;
import com.example.demo.user.exeption.UserNotFoundException;
import com.example.demo.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.07.2020
 */
public class UserServiceTest {

    // private ToDoRepository toDoRepository;

    private UserService userService;

    private UserRepository userRepository;


    //executes before each test defined below
    @BeforeEach
    void setUp() {
        this.userRepository = mock(UserRepository.class);
        userService = new UserService(userRepository);
    }

    @Test
    void whenGetAll_thenReturnAll() {
        //mock
        var testUsers = new ArrayList<User>();
        testUsers.add(new User(UUID.randomUUID(), "user 1"));
        testUsers.add(new User(UUID.randomUUID(), "user 2"));
        when(userRepository.findAllByOrderByNameAsc()).thenReturn(testUsers);

        //call
        var users = userService.getAllUsers();

        //validate
        assertEquals(users.size(), testUsers.size());
        for (int i = 0; i < users.size(); i++) {
            assertThat(users.get(i), samePropertyValuesAs(
                    UserShortDto.fromModel(testUsers.get(i))
            ));
        }
    }


    @Test
    void whenGetByCorrectId_thenReturnCorrectUser() {
        //mock
        var id = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
        var user = new User(id, "user name");

        when(userRepository.findById(ArgumentMatchers.any(UUID.class))).thenReturn(Optional.of(user));

        //call
        var result = userService.getById(id);

        //validate
        assertThat(result, samePropertyValuesAs(
                UserResponseDto.fromModel(user)
        ));

    }

    @Test
    void whenGetByWrongId_thenThrowUserNotFoundException() {
        //mock
        var id = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
        when(userRepository.findById(ArgumentMatchers.any(UUID.class))).thenReturn(Optional.empty());

        //validate
        assertThrows(UserNotFoundException.class, () -> userService.getById(id));
    }

    @Test
    void whenCreateUser_thenReturnCorrectUserShortDto() {
        //mock
        var id = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
        var name = "name";
        var newUser = new User(id, name);
        when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(newUser);

        //call
        var createDto = new UserCreateDto();
        createDto.setName(name);
        var result = userService.createUser(createDto);

        //validate
        assertEquals(result.getName(), name);
        assertNotNull(result.getId());
    }
}
