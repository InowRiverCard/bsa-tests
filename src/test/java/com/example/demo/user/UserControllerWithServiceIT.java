package com.example.demo.user;

import com.example.demo.toDo.model.ToDoEntity;
import com.example.demo.user.exeption.UserNotFoundException;
import com.example.demo.user.model.User;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.07.2020
 */
@ActiveProfiles(profiles = "test")
@WebMvcTest(UserController.class)
@Import(UserService.class)
public class UserControllerWithServiceIT {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {

        String name = "name";
        var id = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
        when(userRepository.findAllByOrderByNameAsc()).thenReturn(
                Collections.singletonList(new User(id, name))
        );

        this.mockMvc
                .perform(get("/user/getAll"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value(name))
                .andExpect(jsonPath("$[0].id").value(id.toString()));
    }


    @Test
    void whenGetById_thenReturnValidResponse() throws Exception {
        String name = "name";
        var id = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
        var firstTask = "fist task";
        var secondTask = "second task";
        var tasks = List.of(new ToDoEntity(0L, firstTask), new ToDoEntity(1L, secondTask));
        when(userRepository.findById(ArgumentMatchers.any(UUID.class)))
                .thenReturn(Optional.of(new User(id, name, tasks)));

        this.mockMvc
                .perform(get("/user/getById/" + id.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.id").value(id.toString()))
                .andExpect(jsonPath("$.tasks").isArray())
                .andExpect(jsonPath("$.tasks", hasSize(2)))
                .andExpect(jsonPath("$.tasks[0].id").isNumber())
                .andExpect(jsonPath("$.tasks[1].id").value(1L))
                .andExpect(jsonPath("$.tasks[0].text").value(firstTask))
                .andExpect(jsonPath("$.tasks[1].text").value(secondTask))
                .andExpect(jsonPath("$.tasks[1].completedAt").doesNotExist());
    }

    @Test
    void whenGetByWrongId_thenReturnNotFound() throws Exception {
        var id = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
        when(userRepository.findById(ArgumentMatchers.any(UUID.class)))
                .thenThrow(new UserNotFoundException("not found"));

        this.mockMvc
                .perform(get("/user/getById/" + id.toString()))
                .andExpect(status().isNotFound())
                .andExpect(content().string("not found"));
    }

    @Test
    void whenCreate_thenReturnValidResponse() throws Exception {
        String name = "name";
        var id = UUID.fromString("d95da7b7-c5c9-479f-a74e-a6523e6cf1b9");
        when(userRepository.save(ArgumentMatchers.any(User.class)))
                .thenReturn(new User(id, name));

        this.mockMvc
                .perform(post("/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"name\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.id").value(id.toString()));
    }

}
