package com.example.demo.user;


import com.example.demo.user.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 13.07.2020
 */
@AutoConfigureMockMvc
@SpringBootTest
public class UserControllerWithServiceAndRepoIT {
    @Autowired
    private MockMvc mockMvc;


    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {

        this.mockMvc
                .perform(get("/user/getAll"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].name").value("Alex"))
                .andExpect(jsonPath("$[3].name").value("Pavel"));
    }

}
