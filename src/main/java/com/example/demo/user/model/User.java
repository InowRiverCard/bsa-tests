package com.example.demo.user.model;

import com.example.demo.toDo.exception.ToDoNotFoundException;
import com.example.demo.toDo.model.ToDoEntity;
import com.example.demo.user.dto.UserCreateDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.07.2020
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    @Type(type = "uuid-char")
    private UUID id;

    @Column
    private String name;

    public User(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ToDoEntity> tasks = new ArrayList<>();

    public void addTask(ToDoEntity toDoEntity) {
        toDoEntity.setUser(this);
        tasks.add(toDoEntity);
    }

    public ToDoEntity getById(Long id) throws ToDoNotFoundException {
        for (ToDoEntity toDo: tasks) {
            if(toDo.getId().equals(id)) {
                return toDo;
            }
        }
        throw new ToDoNotFoundException(id);
    }

    public static User createFromDto(UserCreateDto createDto) {
        return User.builder().name(createDto.getName()).build();
    }
}
