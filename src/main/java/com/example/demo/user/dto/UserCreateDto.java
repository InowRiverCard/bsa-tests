package com.example.demo.user.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.07.2020
 */
@Data
public class UserCreateDto {
    @NotNull
    private String name;
}
