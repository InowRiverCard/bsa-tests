package com.example.demo.user.dto;

import com.example.demo.user.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.07.2020
 */
@AllArgsConstructor
@Data
public class UserShortDto {
    private UUID id;
    private String name;

    public static UserShortDto fromModel(User user) {
        return new UserShortDto(user.getId(), user.getName());
    }
}
