package com.example.demo.user.dto;

import com.example.demo.toDo.dto.ToDoResponse;
import com.example.demo.toDo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.toDo.model.ToDoEntity;
import com.example.demo.user.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.07.2020
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserResponseDto {
    private UUID id;
    private String name;
    private List<ToDoResponse> tasks;

    public static UserResponseDto fromModel(User user) {
        var tasks = user.getTasks().stream()
                .map(ToDoEntityToResponseMapper::map)
                .collect(Collectors.toList());
        return new UserResponseDto(user.getId(), user.getName(), tasks);
    }
}
