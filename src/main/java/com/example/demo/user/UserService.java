package com.example.demo.user;

import com.example.demo.user.dto.UserCreateDto;
import com.example.demo.user.dto.UserResponseDto;
import com.example.demo.user.dto.UserShortDto;
import com.example.demo.user.exeption.UserNotFoundException;
import com.example.demo.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.07.2020
 */
@Service
public class UserService {
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public UserShortDto createUser(UserCreateDto createDto) {
        var user = this.repository.save(User.createFromDto(createDto));
        return UserShortDto.fromModel(user);
    }

    public UserResponseDto getById(UUID id) {
        var user = this.repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Not found user with id=" + id.toString()));
        return UserResponseDto.fromModel(user);
    }

    public List<UserShortDto> getAllUsers() {

        return this.repository.findAllByOrderByNameAsc().stream()
                .map(UserShortDto::fromModel)
                .collect(Collectors.toList());
    }

}
