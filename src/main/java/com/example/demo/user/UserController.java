package com.example.demo.user;

import com.example.demo.user.dto.UserCreateDto;
import com.example.demo.user.dto.UserResponseDto;
import com.example.demo.user.dto.UserShortDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 12.07.2020
 */
@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private UserService service;

    @PostMapping("/create")
    public UserShortDto createUser(@Valid @RequestBody UserCreateDto dto) {
        return service.createUser(dto);
    }

    @GetMapping("/getById/{id}")
    public UserResponseDto findById(@PathVariable UUID id) {
        return service.getById(id);
    }

    @GetMapping("/getAll")
    public List<UserShortDto> getAll() {
        return service.getAllUsers();
    }

}
