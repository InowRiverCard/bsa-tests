package com.example.demo.toDo;

import com.example.demo.toDo.model.ToDoEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ToDoRepository extends JpaRepository<ToDoEntity, Long> {
    @Query("SELECT t FROM ToDoEntity t INNER JOIN t.user u WHERE u.id = :id")
    List<ToDoEntity> findByUserId(UUID id);

    @Query("UPDATE ToDoEntity t SET t.text = :updText where t.id = :taskId " +
            "AND exists (SELECT t FROM ToDoEntity INNER JOIN t.user u WHERE u.id = :userId AND t.id = :taskId)")
    int editUserTask(String userId, String taskId, String updText);


}