package com.example.demo.toDo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ToDoSaveRequest {
	private Long id;

	private UUID userId;

	@NotNull
	private String text;
}