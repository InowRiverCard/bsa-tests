package com.example.demo.toDo.dto.mapper;

import com.example.demo.toDo.dto.ToDoResponse;
import com.example.demo.toDo.model.ToDoEntity;

public class ToDoEntityToResponseMapper {
	public static ToDoResponse map(ToDoEntity todoEntity) {
		if (todoEntity == null)
			return null;
		var result = new ToDoResponse();
		result.setId(todoEntity.getId());
		result.setText(todoEntity.getText());
		result.setCompletedAt(todoEntity.getCompletedAt());
		var userId = todoEntity.getUser() != null ? todoEntity.getUser().getId() : null;
		result.setUserId(userId);
		return result;
	}
}