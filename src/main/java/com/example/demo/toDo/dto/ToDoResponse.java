package com.example.demo.toDo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
public class ToDoResponse {
	@NotNull
	private Long id;

	@NotNull
	private String text;

	@NotNull private UUID userId;

	private ZonedDateTime completedAt;

}