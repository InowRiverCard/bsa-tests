package com.example.demo.toDo;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.example.demo.user.UserRepository;
import com.example.demo.user.exeption.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.toDo.dto.ToDoResponse;
import com.example.demo.toDo.dto.ToDoSaveRequest;
import com.example.demo.toDo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.toDo.exception.ToDoNotFoundException;
import com.example.demo.toDo.model.ToDoEntity;


@Service
public class ToDoService {
	
	private final ToDoRepository toDoRepository;

	private final UserRepository userRepository;

	public ToDoService(ToDoRepository toDoRepository, UserRepository userRepository) {
		this.toDoRepository = toDoRepository;
		this.userRepository = userRepository;
	}

	public List<ToDoResponse> getByUserId(UUID id) {
		return toDoRepository.findByUserId(id).stream()
				.map(ToDoEntityToResponseMapper::map)
				.collect(Collectors.toList());
	}

	public List<ToDoResponse> getAll() {
		return toDoRepository.findAll().stream()
			.map(ToDoEntityToResponseMapper::map)
			.collect(Collectors.toList()); 
	}

	public ToDoResponse upsert(ToDoSaveRequest toDoDTO) throws ToDoNotFoundException, UserNotFoundException {
		ToDoEntity todo;
		//update if it has id or create if it hasn't
		if (toDoDTO.getId() == null) {
			if (toDoDTO.getUserId() == null) {
				throw new UserNotFoundException("User id can`t be null");
			}
			var user = userRepository.findById(toDoDTO.getUserId())
					.orElseThrow(() -> new UserNotFoundException("User not found id=" + toDoDTO.getUserId().toString()));
			todo = new ToDoEntity(toDoDTO.getText());
			user.addTask(todo);
		} else {
			todo = toDoRepository.findById(toDoDTO.getId()).orElseThrow(() -> new ToDoNotFoundException(toDoDTO.getId()));
			todo.setText(toDoDTO.getText());
		}
		return ToDoEntityToResponseMapper.map(toDoRepository.save(todo));
	}

	public ToDoResponse completeToDo(Long id) throws ToDoNotFoundException {
		ToDoEntity todo = toDoRepository.findById(id).orElseThrow(() -> new ToDoNotFoundException(id));
		todo.completeNow();
		return ToDoEntityToResponseMapper.map(toDoRepository.save(todo));
	}

	public ToDoResponse getOne(Long id) throws ToDoNotFoundException {
		return  ToDoEntityToResponseMapper.map(
			toDoRepository.findById(id).orElseThrow(() -> new ToDoNotFoundException(id))
		);
	}

	public void deleteOne(Long id) {
		toDoRepository.deleteById(id);
	}

}
