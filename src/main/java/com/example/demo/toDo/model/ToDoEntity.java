package com.example.demo.toDo.model;
import com.example.demo.user.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * ToDoEntity
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "tasks")
public class ToDoEntity {

	@Id
	@NotNull
	@GeneratedValue
	private Long id;

	@Basic
	@NotNull
	private String text;

	@Basic
	private ZonedDateTime completedAt;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User user;

	public ToDoEntity(String text) {
		this.text = text;
	}

	public ToDoEntity(Long id, String text) {
		this.id = id;
		this.text = text;
	}

	public ZonedDateTime getCompletedAt() {
		return completedAt;
	}
	public ToDoEntity completeNow() {
		completedAt = ZonedDateTime.now(ZoneOffset.UTC);
		return this;
	}
}